# Semantic-UI

We use a customized version of Semantic-UI framework to build our front-end, either Shipup main website (jquery) or Shipup dashboard (react).

## Install documentations

To see the customized version of Semantic-UI documentations, a local server should be run.

Node 8 should be used. It’s recommended to use nvm to change the Node version. Note that nvm switches version just on current session of terminal (current tab) and if you open a new terminal session (new tab) node version on that new session is equal to default version of node on your system.

**Please Read the instructions in the link below to setup semantic-ui and Semantic-UI-Docs projects together**
https://github.com/Semantic-Org/Semantic-UI-Docs

## Running the development server
Set Node version to v8
```
# inside both /docs and /ui folder
nvm use v8
```

Start the docs server:
```
# inside /docs folder
npx docpad run
```

Watch for changes from your /ui folder, and serve to the /docs instance:
```
# inside /ui folder
npx gulp serve-docs
```

## Probable Errors
### Building the documents
`TypeError: Class constructor BasePlugin cannot be invoked without 'new'`

If you encountered this error, see link blow:
https://github.com/docpad/docpad/blob/master/HISTORY.md#v6800-beta-2018-march-7